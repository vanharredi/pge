<article class="module width_full">
    <header><h3><?php echo $title; ?></h3></header>
    <div class="module_content">
        <?php echo $output; ?>
    </div>
</article><!-- end of styles article -->
<div class="spacer"></div>