<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Sistem Arsip PGE Lumut Balai</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/template/css/screen.css" type="text/css" media="screen" title="default" />
</head>
<body id="login-bg">

<!-- Start: login-holder -->
<div id="login-holder">

    <!-- start logo -->
    <div id="logo-login">
        <a href="index.html"></a>
    </div>
    <!-- end logo -->

    <div class="clear"></div>

    <!--  start loginbox ................................................................................. -->
    <div id="loginbox">
        <form action="<?php echo base_url(); ?>login" method="post">
            <!--  start login-inner -->
            <div id="login-inner">
                <table border="0" cellpadding="0" cellspacing="0">
                    <?php  if( isset( $err ) ){ ?>
                        <tr>
                            <th>&nbsp;</th>
                            <td><font color="red"><?php echo $err; ?></font></td>
                        </tr>
                    <?php } ?>
                    <tr>
                        <th>Username</th>
                        <td><input type="text" name="username" class="login-inp" /></td>
                    </tr>
                    <tr>
                        <th>Password</th>
                        <td><input type="password" name="password" class="login-inp" /></td>
                    </tr>
                    <tr>
                        <th>Fungsi</th>
                        <td>
                            <select name="level" class="login-select">
                                <?php
                                    if( $sql_level->num_rows() ){

                                        foreach( $sql_level->result() as $field=>$val )
                                        {

                                            echo '<option class="login-select" value="'. $val->level .'">'. ucfirst( $val->level ) .'</option>';

                                        }

                                    }
                                    else
                                    {

                                        echo '<option class="login-select" value="">==Kosong==</option>';

                                    }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td valign="top">&nbsp;</td>
                    </tr>
                    <tr>
                        <th></th>
                        <td><input type="submit" name="submit" class="submit-login" value="submit" /></td>
                    </tr>
                </table>
            </div>
            <!--  end login-inner -->
        </form>
        <div class="clear"></div>
        <a href="#" class="forgot-pwd">Sistem Arsip PGE Lumut Balai </a>
    </div>
    <!--  end loginbox -->
</div>
<!-- End: login-holder -->
</body>
</html>