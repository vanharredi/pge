<?php
/**
 * Created by PhpStorm.
 * User: gufakto
 * Date: 22/03/2015
 * Time: 12:04
 */

class Manage extends CI_Controller
{


    private $js_files = '';
    private $css_files= '';
    private $menu     = array();

    public function __construct()
    {

        parent::__construct();

        /*Check Session User & Level*/
        $islogin = $this->session->userdata('ISLOGIN');
        if (!$islogin) {
            redirect('login');
        }
        /*End of Check Session ser & Level*/

        $this->js_files = array(
            base_url() . 'assets/template/js/jquery-1.8.3.js',
            base_url() . 'assets/template/js/ui/jquery.ui.core.js',
            base_url() . 'assets/template/js/ui/jquery.ui.widget.js',
            base_url() . 'assets/template/js/ui/jquery.ui.datepicker.js'
        );
        $this->css_files = array(
            base_url() . 'assets/template/js/themes/base/jquery.ui.all.css'
        );

        /*Menu from database*/
        $this->menu = $this->db->query("
                       SELECT * FROM t_fungsi a
                       INNER JOIN t_jenis_arsip b
                       ON a.id_fungsi=b.id_fungsi
                    ");

    }

    public function index()
    {


        $data['sql_menu'] = $this->menu;
        $data['js_files'] = $this->js_files;
        $data['css_files'] = $this->css_files;
        $data['title'] = "Dashboard";
        $data['output'] = "Selamat Datang di Halaman Admin";
        $data['print'] = $this->load->view('output', $data, true);

        $this->load->view('template', $data);

    }

    public function test()
    {
        $incrmnt = $this->db->query( "SELECT MAX(SUBSTRING(id_fungsi,2)) AS id_fungsi FROM t_fungsi" )->row();


        $n2 = "B".str_pad(((int)$incrmnt->id_fungsi + 1), 3, 0, STR_PAD_LEFT);
        echo $n2;

    }

    public function fungsi()
    {

        $crud = new grocery_CRUD();
        $crud->set_table('t_fungsi');
        $crud->set_subject('Fungsi');
        $crud->display_as('id_fungsi', 'Id');
        $crud->display_as('nama_fungsi', 'Nama');
        $crud->required_fields('id_fungsi', 'nama_fungsi');

        $crud->callback_add_field( 'id_fungsi', function(){
            $incrmnt = $this->db->query( "SELECT MAX(SUBSTRING(id_fungsi,2)) AS id_fungsi FROM t_fungsi" )->row();
            return '<input type="text" name="id_fungsi" readonly value="B'. str_pad( (int)$incrmnt->id_fungsi + 1, 3, 0, STR_PAD_LEFT) .'">';
        } );

        $crud->callback_edit_field( 'id_fungsi', function(){
            $incrmnt = $this->db->query( "SELECT MAX(SUBSTRING(id_fungsi,2)) AS id_fungsi FROM t_fungsi" )->row();
            return '<input type="text" name="id_fungsi" readonly value="B'. str_pad( (int)$incrmnt->id_fungsi, 3, 0, STR_PAD_LEFT) .'">';
        } );

        $result_crud = $crud->render();

        $data['sql_menu'] = $this->menu;
        $data['title'] = "Fungsi";
        $data['js_files'] = $result_crud->js_files;
        $data['css_files'] = $result_crud->css_files;
        $data['output'] = $result_crud->output;
        $data['print'] = $this->load->view('output', $data, true);

        $this->load->view('template', $data);

    }

    public function sub_fungsi_arsip()
    {

        $crud = new grocery_CRUD();
        $crud->set_table('t_jenis_arsip');
        $crud->set_subject('Jenis Arsip');
        $crud->set_relation('id_fungsi', 't_fungsi', 'nama_fungsi');
        $crud->display_as('jenis_surat', 'Jenis Arsip');
        $crud->display_as('id_fungsi', 'Fungsi');
        $crud->required_fields('jenis_surat', 'id_fungsi');

        $result_crud = $crud->render();

        $data['sql_menu'] = $this->menu;
        $data['title'] = "Jenis Arsip";
        $data['js_files'] = $result_crud->js_files;
        $data['css_files'] = $result_crud->css_files;
        $data['output'] = $result_crud->output;
        $data['print'] = $this->load->view('output', $data, true);

        $this->load->view('template', $data);

    }

    public function akun()
    {

        $sql_lvl  = $this->db->query( "SELECT level FROM t_user GROUP BY level" );
        $fiel_lvl = array();
        if( $sql_lvl->num_rows() )
        {

            foreach( $sql_lvl->result() as $flvl => $vlvl )
            {

                $fiel_lvl[ $vlvl->level ] = $vlvl->level;

            }

        }

        $crud = new grocery_CRUD();
        $crud->set_table( 't_user' );
        $crud->set_subject( 'User' );
        $crud->set_relation( 'id_fungsi', 't_fungsi', 'nama_fungsi' );
        $crud->display_as( 'id_fungsi', 'Fungsi' );
        $crud->columns( 'username', 'id_fungsi', 'level' );
        $crud->change_field_type( 'password', 'password' );
        $crud->field_type( 'level', 'dropdown', $fiel_lvl );
        $crud->required_fields( 'username', 'password', 'id_fungsi', 'level' );
        $crud->callback_before_insert( array( $this, 'encrypt_password_callback' ) );
        $crud->callback_before_update( array( $this, 'encrypt_password_callback' ) );

        $result_crud = $crud->render();

        $data[ 'sql_menu' ] = $this->menu;
        $data[ 'title' ]    = "Manage Akun";
        $data[ 'js_files' ] = $result_crud->js_files;
        $data[ 'css_files' ]= $result_crud->css_files;
        $data[ 'output' ]   = $result_crud->output;
        $data[ 'print' ]    = $this->load->view('output', $data, true);

        $this->load->view('template', $data);

    }

    /*Callback encript password before save to database and has to md5 encription*/
    public function encrypt_password_callback( $post )
    {

        $post[ 'password' ] = md5( $post[ 'password' ] );

        return $post;

    }

    /*Detail arsip*/
    public function detail_arsip( $id )
    {

        $crud = new grocery_CRUD();
        $crud->where('id_jenis_arsip',$id);
        $crud->set_table( 't_agenda_surat_masuk' );
        $crud->set_subject( 'Arsip' );
//        $crud->set_relation( 'no_file_surat', 't_file_arsip_masuk', 'nama_file' );
        $crud->display_as( 'no_file_surat', 'File' );
        $crud->display_as( 'tujuan_srt_masuk', 'Tujuan' );
        $crud->display_as( 'perihal_surat_masuk', 'Perihal' );
        $crud->display_as( 'nmr_surat', 'Nomor Surat' );
        $crud->display_as( 'tgl_agenda', 'Taggal Agenda' );
        $crud->display_as( 'tgl_surat', 'Taggal Surat' );
        $crud->columns( 'no_agenda', 'pengirim_surat', 'perihal_surat_masuk', 'tujuan_srt_masuk', 'tgl_agenda', 'tgl_surat', 'nmr_surat' , 'no_file_surat');
        $crud->unset_delete();
        $crud->unset_add();
        $crud->unset_edit();
        $crud->unset_read();
        $crud->callback_before_insert( array( $this, 'encrypt_password_callback' ) );
        $crud->callback_column( 'no_file_surat', array( $this, '_callback_column_no_file_surat' ) );


        $result_crud = $crud->render();

        $data[ 'sql_menu' ] = $this->menu;
        $data[ 'title' ]    = "Detail Arsip";
        $data[ 'js_files' ] = $result_crud->js_files;
        $data[ 'css_files' ]= $result_crud->css_files;
        $data[ 'output' ]   = $result_crud->output;
        $data[ 'print' ]    = $this->load->view('output', $data, true);

        $this->load->view('template', $data);

    }

    public function _callback_column_no_file_surat( $v, $r )
    {

        $sql_file = $this->db->query( "SELECT * FROM t_file_arsip_masuk WHERE no_file_surat = '". $r->no_file_surat ."'" );
        if( $sql_file->num_rows() )
        {
            $file = $sql_file->row();
            return '<a href="'. base_url() .'uploads/files/'. $file->nama_file .'" target="_blank">Download</a>';
        }
        else
        {
            return "";
        }
    }


    public function penerimaan_surat( $id_fungsi='' )
    {

        $id_pk = '';

        $crud = new grocery_CRUD();

        if( !empty( $id_fungsi ) )
        {

            $this->db->select( 'id_jenis_arsip' );
            $this->db->where( 'id_fungsi', $id_fungsi );
            $data_arsip = $this->db->get( 't_jenis_arsip' );

            if( $data_arsip->num_rows() )
            {

                $res_arsip = $data_arsip->row();
                $id_pk = $res_arsip->id_jenis_arsip;

            }

        }

        if( !empty( $id_fungsi ) ):
            $crud->where( 'j1658473c.id_jenis_arsip', $id_pk );
        endif;
        $crud->set_table( 't_agenda_surat_masuk' );
        $crud->set_subject( 'Arsip' );
        $crud->set_relation( 'id_jenis_arsip', 't_jenis_arsip', 'jenis_surat' );
        $crud->display_as( 'no_file_surat', 'File' );
        $crud->display_as( 'tujuan_srt_masuk', 'Tujuan' );
        $crud->display_as( 'perihal_surat_masuk', 'Perihal' );
        $crud->display_as( 'nmr_surat', 'Nomor Surat' );
        $crud->display_as( 'tgl_agenda', 'Taggal Agenda' );
        $crud->display_as( 'tgl_surat', 'Taggal Surat' );
        $crud->display_as( 'id_jenis_arsip', 'Fungsi' );
        $crud->required_fields( 'no_agenda', 'pengirim_surat', 'perihal_surat_masuk', 'tujuan_srt_masuk', 'tgl_agenda', 'tgl_surat', 'nmr_surat' , 'no_file_surat', 'id_jenis_arsip');
        $crud->set_field_upload( 'no_file_surat', 'assets/uploads/files' );
        $crud->unset_delete();

        $crud->unset_edit();
        if( empty( $id_fungsi ) )
        {

            $crud->unset_read();
            $crud->unset_list();
            $crud->unset_back_to_list();

        }
        else
        {
            $crud->unset_add();
        }

        $crud->callback_before_insert( array( $this, '_callback_before_insert_agenda' ) );
        $crud->callback_column( 'no_file_surat', array( $this, '_callback_column_no_file_surat' ) );
        $crud->callback_before_upload(array($this,'_callback_before_upload_arsip_masuk'));
        $crud->callback_after_upload(array($this,'_callback_after_upload_arsip_masuk'));


        $result_crud = $crud->render();

        $data[ 'sql_menu' ] = $this->menu;
        $data[ 'title' ]    = "Penerimaan Arsip Arsip";
        $data[ 'js_files' ] = $result_crud->js_files;
        $data[ 'css_files' ]= $result_crud->css_files;
        $data[ 'output' ]   = $result_crud->output;
        $data[ 'print' ]    = $this->load->view('output', $data, true);


        $this->load->view('template', $data);

    }

    public function _callback_before_insert_agenda( $post_array )
    {

        $sql_id = $this->db->get_where( 't_file_arsip_masuk', array('nama_file'=>$post_array[ 'no_file_surat' ]) )->row();
        $post_array[ 'no_file_surat' ] = $sql_id->no_file_surat;
        return $post_array;

    }

    public function _callback_before_upload_arsip_masuk( $files_to_upload,$field_info )
    {

        foreach( $files_to_upload as $k ) {
            $sql_filearsip = array(
                'nama_file' => $k['name'],
                'ukuran'    => $k['size'],
                'type'      => $k['type'],
                'tgl_surat' => ''
            );
        }

        $this->db->insert( 't_file_arsip_masuk .', $sql_filearsip );

        return true;

    }

    public function _callback_after_upload_arsip_masuk( $uploader_response,$field_info, $files_to_upload )
    {

        foreach ($files_to_upload as $k) {
            $where = array(
                'nama_file' => $k['name'],
                'ukuran'    => $k['size'],
                'type'      => $k['type']
            );
        }

        $query  = $this->db->get_where('t_file_arsip_masuk', $where)->row();

        $update = array(
            'nama_file' => $uploader_response[0]->name
        );

        $this->db->where( 'no_file_surat', $query->no_file_surat );
        $this->db->update( 't_file_arsip_masuk',$update );

        return true;
    }

    public function surat_keluar()
    {

        $crud = new grocery_CRUD();
        $crud->set_table( 't_agenda_surat_keluar' );
        $crud->set_subject( 'Surat Keluar' );
        $crud->set_relation( 'id_jenis_arsip', 't_jenis_arsip', 'jenis_surat' );
        $crud->display_as( 'no_surat', 'Nomor Surat' );
        $crud->display_as( 'pengirim_srt_klr', 'Pengirim' );
        $crud->display_as( 'tujuan_srt', 'Tujuan' );
        $crud->display_as( 'tgl_agd', 'Taggal Agenda' );
        $crud->display_as( 'isisurat', 'File' );
        $crud->display_as( 'id_jenis_arsip', 'Fungsi' );
        $crud->required_fields( 'id_jenis_arsip', 'no_surat', 'pengirim_srt_klr', 'tujuan_srt', 'tgl_agd', 'isisurat');
        $crud->set_field_upload( 'isisurat', 'assets/uploads/files' );
        $crud->unset_delete();
        $crud->unset_add();
        $crud->unset_edit();
        if( $this->session->userdata( 'LEVEL' ) != 'admin' )
        {

            $crud->add_action('Ubah Surat Keluar', base_url().'assets/grocery_crud/themes/flexigrid/css/images/edit.png', 'manage/surat_keluar_action/edit', 'edit_button' );

        }

        $result_crud = $crud->render();

        $data[ 'sql_menu' ] = $this->menu;
        $data[ 'title' ]    = "Penerimaan Arsip Arsip";
        $data[ 'js_files' ] = $result_crud->js_files;
        $data[ 'css_files' ]= $result_crud->css_files;
        $data[ 'output' ]   = $result_crud->output;
        $data[ 'print' ]    = $this->load->view('output', $data, true);


        $this->load->view('template', $data);

    }

    public function surat_keluar_action()
    {

        $crud = new grocery_CRUD();
        $crud->set_table( 't_agenda_surat_keluar' );
        $crud->set_subject( 'Surat Keluar' );
        $crud->set_relation( 'id_jenis_arsip', 't_jenis_arsip', 'jenis_surat' );
        $crud->display_as( 'no_surat', 'Nomor Surat' );
        $crud->display_as( 'pengirim_srt_klr', 'Pengirim' );
        $crud->display_as( 'tujuan_srt', 'Tujuan' );
        $crud->display_as( 'tgl_agd', 'Taggal Agenda' );
        $crud->display_as( 'isisurat', 'File' );
        $crud->display_as( 'id_jenis_arsip', 'Fungsi' );
        $crud->required_fields( 'id_jenis_arsip', 'no_surat', 'pengirim_srt_klr', 'tujuan_srt', 'tgl_agd', 'isisurat');
        $crud->set_field_upload( 'isisurat', 'assets/uploads/files' );
        $crud->unset_list();
        $crud->unset_read();
        $crud->unset_back_to_list();

        if( $this->session->userdata( 'LEVEL' ) != 'admin' )
        {

            $crud->callback_field( 'id_jenis_arsip', function( $v, $r){

                $this->db->select( 'id_jenis_arsip, jenis_surat' );
                $this->db->where( 'id_fungsi', $this->session->userdata( 'FUNGSI' ) );
                $data_arsip = $this->db->get( 't_jenis_arsip' );
                $id_jenis_arsip = "";
                $jenis_arsip    = "";

                if( $data_arsip->num_rows() )
                {

                    $res_arsip       = $data_arsip->row();
                    $id_jenis_arsip  = $res_arsip->id_jenis_arsip;
                    $jenis_arsip     = $res_arsip->jenis_surat;

                }

                return '<input type="hidden" name="id_jenis_arsip" value="'. $id_jenis_arsip .'">
                        <input type="text" name="nama_jenis_arsip" readonly value="'. $jenis_arsip .'">';
            } );

        }

        $result_crud = $crud->render();

        $data[ 'sql_menu' ] = $this->menu;
        $data[ 'title' ]    = "Penerimaan Arsip Arsip";
        $data[ 'js_files' ] = $result_crud->js_files;
        $data[ 'css_files' ]= $result_crud->css_files;
        $data[ 'output' ]   = '<a href="'. base_url() .'manage/surat_keluar" class="btn btn-sm btn-primary">Kembali</a>'.$result_crud->output;
        $data[ 'print' ]    = $this->load->view('output', $data, true);


        $this->load->view('template', $data);

    }

    public function profil()
    {

        $crud = new grocery_CRUD();
        $crud->set_table( 't_user' );
        $crud->set_subject( 'Profil' );
        $crud->set_relation( 'id_fungsi', 't_fungsi', 'nama_fungsi' );
        $crud->display_as( 'id_fungsi', 'Fungsi' );
        $crud->columns( 'username', 'id_fungsi', 'level' );
        $crud->change_field_type( 'password', 'password' );
        $crud->fields( 'username', 'password');
        $crud->callback_edit_field('username', function( $v, $r ){
            return '<input type="text" name="username" readonly value="'. $v .'">';
        });
        $crud->required_fields( 'username', 'password', 'id_fungsi', 'level' );
        $crud->callback_before_insert( array( $this, 'encrypt_password_callback' ) );
        $crud->unset_back_to_list();
        $crud->unset_read();
        $crud->unset_add();
        $crud->unset_list();
        $crud->unset_delete();

        $result_crud = $crud->render();

        $data[ 'sql_menu' ] = $this->menu;
        $data[ 'title' ]    = "Manage Akun";
        $data[ 'js_files' ] = $result_crud->js_files;
        $data[ 'css_files' ]= $result_crud->css_files;
        $data[ 'output' ]   = $result_crud->output;
        $data[ 'print' ]    = $this->load->view('output', $data, true);

        $this->load->view('template', $data);

    }

} 