/*
SQLyog Enterprise - MySQL GUI v8.05 
MySQL - 5.5.34 : Database - pge
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`pge` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `pge`;

/*Table structure for table `daemons` */

DROP TABLE IF EXISTS `daemons`;

CREATE TABLE `daemons` (
  `Start` text NOT NULL,
  `Info` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `daemons` */

/*Table structure for table `gammu` */

DROP TABLE IF EXISTS `gammu`;

CREATE TABLE `gammu` (
  `Version` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `gammu` */

insert  into `gammu`(`Version`) values (10);

/*Table structure for table `inbox` */

DROP TABLE IF EXISTS `inbox`;

CREATE TABLE `inbox` (
  `UpdatedInDB` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ReceivingDateTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Text` text NOT NULL,
  `SenderNumber` varchar(20) NOT NULL DEFAULT '',
  `Coding` enum('Default_No_Compression','Unicode_No_Compression','8bit','Default_Compression','Unicode_Compression') NOT NULL DEFAULT 'Default_No_Compression',
  `UDH` text NOT NULL,
  `SMSCNumber` varchar(20) NOT NULL DEFAULT '',
  `Class` int(11) NOT NULL DEFAULT '-1',
  `TextDecoded` text NOT NULL,
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `RecipientID` text NOT NULL,
  `Processed` enum('false','true') NOT NULL DEFAULT 'false',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `inbox` */

/*Table structure for table `lampiran_sk` */

DROP TABLE IF EXISTS `lampiran_sk`;

CREATE TABLE `lampiran_sk` (
  `no_surat` varchar(100) NOT NULL,
  `lampiran` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`no_surat`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `lampiran_sk` */

insert  into `lampiran_sk`(`no_surat`,`lampiran`) values ('D004/001/2012','Semua Lampiran di isi disini'),('D001/002/2012','Semua Lampiran di isi disini'),('D002/003/2012','Melampirkan Surat Keterangan'),('D008/004/2012','Apa ajalah'),('D010/005/2012','asdadasda');

/*Table structure for table `outbox` */

DROP TABLE IF EXISTS `outbox`;

CREATE TABLE `outbox` (
  `UpdatedInDB` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `InsertIntoDB` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `SendingDateTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Text` text,
  `DestinationNumber` varchar(20) NOT NULL DEFAULT '',
  `Coding` enum('Default_No_Compression','Unicode_No_Compression','8bit','Default_Compression','Unicode_Compression') NOT NULL DEFAULT 'Default_No_Compression',
  `UDH` text,
  `Class` int(11) DEFAULT '-1',
  `TextDecoded` text NOT NULL,
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `MultiPart` enum('false','true') DEFAULT 'false',
  `RelativeValidity` int(11) DEFAULT '-1',
  `SenderID` varchar(255) DEFAULT NULL,
  `SendingTimeOut` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `DeliveryReport` enum('default','yes','no') DEFAULT 'default',
  `CreatorID` text NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `outbox_date` (`SendingDateTime`,`SendingTimeOut`),
  KEY `outbox_sender` (`SenderID`)
) ENGINE=MyISAM AUTO_INCREMENT=145 DEFAULT CHARSET=utf8;

/*Data for the table `outbox` */

insert  into `outbox`(`UpdatedInDB`,`InsertIntoDB`,`SendingDateTime`,`Text`,`DestinationNumber`,`Coding`,`UDH`,`Class`,`TextDecoded`,`ID`,`MultiPart`,`RelativeValidity`,`SenderID`,`SendingTimeOut`,`DeliveryReport`,`CreatorID`) values ('2012-06-02 22:17:29','2012-06-02 22:17:29','0000-00-00 00:00:00',NULL,'081397932320','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat dengan Nomor Surat  ',15,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-06-03 12:27:39','2012-06-03 12:27:39','0000-00-00 00:00:00',NULL,'081397932320','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat dengan Nomor Surat  ',16,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-06-03 12:28:19','2012-06-03 12:28:19','0000-00-00 00:00:00',NULL,'081397932320','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat dengan Nomor Surat  ',17,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-06-03 12:30:22','2012-06-03 12:30:22','0000-00-00 00:00:00',NULL,'087791263256','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',18,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-06-03 12:30:22','2012-06-03 12:30:22','0000-00-00 00:00:00',NULL,'081397932320','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',19,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-06-03 12:30:22','2012-06-03 12:30:22','0000-00-00 00:00:00',NULL,'','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',20,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-06-03 12:36:37','2012-06-03 12:36:37','0000-00-00 00:00:00',NULL,'087791263256','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',21,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-06-03 12:36:37','2012-06-03 12:36:37','0000-00-00 00:00:00',NULL,'081397932320','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',22,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-06-03 12:36:37','2012-06-03 12:36:37','0000-00-00 00:00:00',NULL,'081397932320','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',23,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-06-03 12:36:37','2012-06-03 12:36:37','0000-00-00 00:00:00',NULL,'','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',24,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-06-03 12:38:43','2012-06-03 12:38:43','0000-00-00 00:00:00',NULL,'087791263256','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',25,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-06-03 16:19:51','2012-06-03 16:19:51','0000-00-00 00:00:00',NULL,'081397932320','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat dengan Nomor Surat  ',26,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-06-03 16:23:05','2012-06-03 16:23:05','0000-00-00 00:00:00',NULL,'087791263256','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',27,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-06-03 16:23:05','2012-06-03 16:23:05','0000-00-00 00:00:00',NULL,'081397932320','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',28,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-06-03 16:23:05','2012-06-03 16:23:05','0000-00-00 00:00:00',NULL,'','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',29,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-06-03 16:23:05','2012-06-03 16:23:05','0000-00-00 00:00:00',NULL,'081397932320','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',30,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-06-03 16:23:05','2012-06-03 16:23:05','0000-00-00 00:00:00',NULL,'','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',31,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-06-03 16:23:05','2012-06-03 16:23:05','0000-00-00 00:00:00',NULL,'','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',32,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-06-03 16:23:05','2012-06-03 16:23:05','0000-00-00 00:00:00',NULL,'','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',33,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-06-03 16:23:05','2012-06-03 16:23:05','0000-00-00 00:00:00',NULL,'','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',34,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-06-03 16:23:05','2012-06-03 16:23:05','0000-00-00 00:00:00',NULL,'','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',35,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-06-03 16:23:05','2012-06-03 16:23:05','0000-00-00 00:00:00',NULL,'','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',36,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-06-03 16:23:05','2012-06-03 16:23:05','0000-00-00 00:00:00',NULL,'','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',37,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-06-06 18:36:51','2012-06-06 18:36:51','0000-00-00 00:00:00',NULL,'081397932320','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat dengan Nomor Surat  ',38,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-06-06 18:38:22','2012-06-06 18:38:22','0000-00-00 00:00:00',NULL,'087791263256','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',39,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-06-06 18:38:22','2012-06-06 18:38:22','0000-00-00 00:00:00',NULL,'081397932320','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',40,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-06-06 18:38:22','2012-06-06 18:38:22','0000-00-00 00:00:00',NULL,'','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',41,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-06-06 18:38:22','2012-06-06 18:38:22','0000-00-00 00:00:00',NULL,'081397932320','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',42,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-06-06 18:38:22','2012-06-06 18:38:22','0000-00-00 00:00:00',NULL,'','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',43,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-06-06 18:38:22','2012-06-06 18:38:22','0000-00-00 00:00:00',NULL,'','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',44,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-06-06 18:38:22','2012-06-06 18:38:22','0000-00-00 00:00:00',NULL,'','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',45,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-06-06 18:38:22','2012-06-06 18:38:22','0000-00-00 00:00:00',NULL,'','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',46,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-06-06 18:38:22','2012-06-06 18:38:22','0000-00-00 00:00:00',NULL,'','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',47,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-06-06 18:38:22','2012-06-06 18:38:22','0000-00-00 00:00:00',NULL,'','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',48,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-06-06 18:38:22','2012-06-06 18:38:22','0000-00-00 00:00:00',NULL,'','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',49,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-07-25 14:49:28','2012-07-25 14:49:28','0000-00-00 00:00:00',NULL,'081397932320','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat dengan Nomor Surat  ',50,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-07-25 15:21:37','2012-07-25 15:21:37','0000-00-00 00:00:00',NULL,'087791263256','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',51,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-07-25 15:21:37','2012-07-25 15:21:37','0000-00-00 00:00:00',NULL,'','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',52,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-07-25 15:21:37','2012-07-25 15:21:37','0000-00-00 00:00:00',NULL,'081397932320','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',53,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-07-25 15:21:37','2012-07-25 15:21:37','0000-00-00 00:00:00',NULL,'','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',54,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-07-28 14:37:51','2012-07-28 14:37:51','0000-00-00 00:00:00',NULL,'085758665157','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat dengan Nomor Surat  ',55,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-07-29 00:10:14','2012-07-29 00:10:14','0000-00-00 00:00:00',NULL,'085758665157','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',56,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-07-29 00:10:14','2012-07-29 00:10:14','0000-00-00 00:00:00',NULL,'085758665157','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',57,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-07-29 14:10:35','2012-07-29 14:10:35','0000-00-00 00:00:00',NULL,'085758665157','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat dengan Nomor Surat  ',58,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-07-29 14:26:15','2012-07-29 14:26:15','0000-00-00 00:00:00',NULL,'085758665157','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',59,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-07-29 14:26:16','2012-07-29 14:26:16','0000-00-00 00:00:00',NULL,'085758665157','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',60,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-07-29 14:26:16','2012-07-29 14:26:16','0000-00-00 00:00:00',NULL,'085758665157','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',61,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-07-29 22:46:18','2012-07-29 22:46:18','0000-00-00 00:00:00',NULL,'085758665157','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat dengan Nomor Surat  ',62,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-07-29 22:47:23','2012-07-29 22:47:23','0000-00-00 00:00:00',NULL,'085758665157','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',63,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-07-29 22:47:23','2012-07-29 22:47:23','0000-00-00 00:00:00',NULL,'085758665157','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',64,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-07-29 22:47:23','2012-07-29 22:47:23','0000-00-00 00:00:00',NULL,'085758665157','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',65,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-07-29 22:47:23','2012-07-29 22:47:23','0000-00-00 00:00:00',NULL,'085758665157','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',66,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-07-29 22:47:23','2012-07-29 22:47:23','0000-00-00 00:00:00',NULL,'085758665157','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',67,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-07-29 22:47:23','2012-07-29 22:47:23','0000-00-00 00:00:00',NULL,'085758665157','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',68,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-07-29 22:47:23','2012-07-29 22:47:23','0000-00-00 00:00:00',NULL,'085758665157','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',69,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-07-29 22:47:23','2012-07-29 22:47:23','0000-00-00 00:00:00',NULL,'085758665157','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',70,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-07-29 22:47:23','2012-07-29 22:47:23','0000-00-00 00:00:00',NULL,'085758665157','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',71,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-07-29 22:47:23','2012-07-29 22:47:23','0000-00-00 00:00:00',NULL,'085758665157','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',72,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-07-29 22:47:23','2012-07-29 22:47:23','0000-00-00 00:00:00',NULL,'085758665157','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',73,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-08-04 20:25:18','2012-08-04 20:25:18','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat dengan Nomor Surat  ',74,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-08-04 21:21:51','2012-08-04 21:21:51','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',75,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-08-04 21:21:51','2012-08-04 21:21:51','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',76,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-08-04 21:21:51','2012-08-04 21:21:51','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',77,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-08-04 21:21:51','2012-08-04 21:21:51','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',78,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-08-04 21:21:51','2012-08-04 21:21:51','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',79,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-08-04 21:21:51','2012-08-04 21:21:51','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',80,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-08-04 21:21:51','2012-08-04 21:21:51','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',81,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-08-04 21:21:51','2012-08-04 21:21:51','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',82,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-08-04 21:21:51','2012-08-04 21:21:51','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',83,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-08-04 21:21:51','2012-08-04 21:21:51','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',84,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-08-04 21:51:19','2012-08-04 21:51:19','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',85,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-08-04 22:36:40','2012-08-04 22:36:40','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',86,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-08-04 22:56:39','2012-08-04 22:56:39','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',87,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-08-05 12:08:15','2012-08-05 12:08:15','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',88,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-08-05 12:52:41','2012-08-05 12:52:41','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat dengan Nomor Surat  ',89,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-08-05 12:54:20','2012-08-05 12:54:20','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',90,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-08-05 12:54:20','2012-08-05 12:54:20','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',91,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-08-05 12:54:20','2012-08-05 12:54:20','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',92,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-08-05 12:54:20','2012-08-05 12:54:20','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',93,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-08-05 12:54:20','2012-08-05 12:54:20','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',94,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-08-05 12:54:20','2012-08-05 12:54:20','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',95,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-08-05 12:54:20','2012-08-05 12:54:20','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',96,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-08-05 12:54:20','2012-08-05 12:54:20','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',97,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-08-05 12:54:20','2012-08-05 12:54:20','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',98,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-08-05 12:54:20','2012-08-05 12:54:20','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',99,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-08-05 12:55:50','2012-08-05 12:55:50','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',100,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-08-05 14:29:53','2012-08-05 14:29:53','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat dengan Nomor Surat  ',101,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-08-05 14:31:46','2012-08-05 14:31:46','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',102,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-08-05 14:31:46','2012-08-05 14:31:46','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',103,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-08-05 14:33:04','2012-08-05 14:33:04','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',104,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-08-06 05:06:02','2012-08-06 05:06:02','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat dengan Nomor Surat  ',105,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-08-06 14:25:40','2012-08-06 14:25:40','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',106,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-08-06 14:26:18','2012-08-06 14:26:18','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',107,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-08-08 13:26:03','2012-08-08 13:26:03','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat dengan Nomor Surat  ',108,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-08-12 20:12:58','2012-08-12 20:12:58','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',109,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-08-12 20:22:51','2012-08-12 20:22:51','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat dengan Nomor Surat  ',110,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-08-12 20:23:52','2012-08-12 20:23:52','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',111,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-08-12 20:24:49','2012-08-12 20:24:49','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',112,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-08-12 21:58:42','2012-08-12 21:58:42','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat dengan Nomor Surat  ',113,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-08-12 22:10:48','2012-08-12 22:10:48','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat dengan Nomor Surat  ',114,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-08-30 15:15:33','2012-08-30 15:15:33','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat dengan Nomor Surat  ',115,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-08-30 15:17:52','2012-08-30 15:17:52','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat dengan Nomor Surat  ',116,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-08-30 15:18:14','2012-08-30 15:18:14','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat dengan Nomor Surat  ',117,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-08-30 15:19:26','2012-08-30 15:19:26','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',118,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-08-30 15:23:46','2012-08-30 15:23:46','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',119,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-08-30 15:23:46','2012-08-30 15:23:46','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',120,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-08-30 15:23:58','2012-08-30 15:23:58','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',121,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-08-30 15:25:07','2012-08-30 15:25:07','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',122,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-09-20 20:45:29','2012-09-20 20:45:29','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',123,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-09-20 20:45:29','2012-09-20 20:45:29','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',124,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-09-20 20:45:29','2012-09-20 20:45:29','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',125,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-09-20 20:45:29','2012-09-20 20:45:29','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',126,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-09-20 20:45:29','2012-09-20 20:45:29','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',127,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-09-20 20:45:29','2012-09-20 20:45:29','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',128,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-09-20 20:45:29','2012-09-20 20:45:29','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',129,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-09-20 20:45:29','2012-09-20 20:45:29','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',130,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-09-20 20:45:29','2012-09-20 20:45:29','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',131,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-09-20 20:45:29','2012-09-20 20:45:29','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',132,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-11-06 22:49:22','2012-11-06 22:49:22','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat dengan Nomor Surat  ',133,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-11-06 22:53:59','2012-11-06 22:53:59','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat dengan Nomor Surat  ',134,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-11-06 22:58:56','2012-11-06 22:58:56','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',135,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-11-06 22:58:56','2012-11-06 22:58:56','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',136,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-11-06 22:58:56','2012-11-06 22:58:56','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',137,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-11-06 22:58:56','2012-11-06 22:58:56','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',138,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-11-06 22:58:56','2012-11-06 22:58:56','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',139,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-11-06 22:58:56','2012-11-06 22:58:56','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',140,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-11-06 22:58:56','2012-11-06 22:58:56','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',141,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-11-06 22:58:56','2012-11-06 22:58:56','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',142,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-11-06 22:58:56','2012-11-06 22:58:56','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',143,'false',-1,NULL,'0000-00-00 00:00:00','default',''),('2012-11-06 22:58:56','2012-11-06 22:58:56','0000-00-00 00:00:00',NULL,'089627185727','Default_No_Compression',NULL,-1,'Silahkan Cek akun anda, Ada surat yang harus anda tidak lanjuti ',144,'false',-1,NULL,'0000-00-00 00:00:00','default','');

/*Table structure for table `outbox_multipart` */

DROP TABLE IF EXISTS `outbox_multipart`;

CREATE TABLE `outbox_multipart` (
  `Text` text,
  `Coding` enum('Default_No_Compression','Unicode_No_Compression','8bit','Default_Compression','Unicode_Compression') NOT NULL DEFAULT 'Default_No_Compression',
  `UDH` text,
  `Class` int(11) DEFAULT '-1',
  `TextDecoded` text,
  `ID` int(10) unsigned NOT NULL DEFAULT '0',
  `SequencePosition` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`,`SequencePosition`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `outbox_multipart` */

/*Table structure for table `pbk` */

DROP TABLE IF EXISTS `pbk`;

CREATE TABLE `pbk` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `GroupID` int(11) NOT NULL DEFAULT '-1',
  `Name` text NOT NULL,
  `Number` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `pbk` */

/*Table structure for table `pbk_groups` */

DROP TABLE IF EXISTS `pbk_groups`;

CREATE TABLE `pbk_groups` (
  `Name` text NOT NULL,
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `pbk_groups` */

/*Table structure for table `pesan_revisi` */

DROP TABLE IF EXISTS `pesan_revisi`;

CREATE TABLE `pesan_revisi` (
  `kd_psn` int(11) NOT NULL AUTO_INCREMENT,
  `id_agnda` varchar(100) NOT NULL,
  `tgl` varchar(100) NOT NULL,
  `isi` text NOT NULL,
  PRIMARY KEY (`kd_psn`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `pesan_revisi` */

/*Table structure for table `phones` */

DROP TABLE IF EXISTS `phones`;

CREATE TABLE `phones` (
  `ID` text NOT NULL,
  `UpdatedInDB` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `InsertIntoDB` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `TimeOut` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Send` enum('yes','no') NOT NULL DEFAULT 'no',
  `Receive` enum('yes','no') NOT NULL DEFAULT 'no',
  `IMEI` varchar(35) NOT NULL,
  `Client` text NOT NULL,
  `Battery` int(11) NOT NULL DEFAULT '0',
  `Signal` int(11) NOT NULL DEFAULT '0',
  `Sent` int(11) NOT NULL DEFAULT '0',
  `Received` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`IMEI`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `phones` */

insert  into `phones`(`ID`,`UpdatedInDB`,`InsertIntoDB`,`TimeOut`,`Send`,`Receive`,`IMEI`,`Client`,`Battery`,`Signal`,`Sent`,`Received`) values ('','2012-05-28 12:58:21','2012-05-28 12:54:06','2012-05-28 12:58:31','yes','yes','353142047371110','Gammu 1.25.0, Windows Server 2007, GCC 4.3, MinGW 3.15',0,57,4,0);

/*Table structure for table `sentitems` */

DROP TABLE IF EXISTS `sentitems`;

CREATE TABLE `sentitems` (
  `UpdatedInDB` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `InsertIntoDB` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `SendingDateTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `DeliveryDateTime` timestamp NULL DEFAULT NULL,
  `Text` text NOT NULL,
  `DestinationNumber` varchar(20) NOT NULL DEFAULT '',
  `Coding` enum('Default_No_Compression','Unicode_No_Compression','8bit','Default_Compression','Unicode_Compression') NOT NULL DEFAULT 'Default_No_Compression',
  `UDH` text NOT NULL,
  `SMSCNumber` varchar(20) NOT NULL DEFAULT '',
  `Class` int(11) NOT NULL DEFAULT '-1',
  `TextDecoded` text NOT NULL,
  `ID` int(10) unsigned NOT NULL DEFAULT '0',
  `SenderID` varchar(255) NOT NULL,
  `SequencePosition` int(11) NOT NULL DEFAULT '1',
  `Status` enum('SendingOK','SendingOKNoReport','SendingError','DeliveryOK','DeliveryFailed','DeliveryPending','DeliveryUnknown','Error') NOT NULL DEFAULT 'SendingOK',
  `StatusError` int(11) NOT NULL DEFAULT '-1',
  `TPMR` int(11) NOT NULL DEFAULT '-1',
  `RelativeValidity` int(11) NOT NULL DEFAULT '-1',
  `CreatorID` text NOT NULL,
  PRIMARY KEY (`ID`,`SequencePosition`),
  KEY `sentitems_date` (`DeliveryDateTime`),
  KEY `sentitems_tpmr` (`TPMR`),
  KEY `sentitems_dest` (`DestinationNumber`),
  KEY `sentitems_sender` (`SenderID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `sentitems` */

insert  into `sentitems`(`UpdatedInDB`,`InsertIntoDB`,`SendingDateTime`,`DeliveryDateTime`,`Text`,`DestinationNumber`,`Coding`,`UDH`,`SMSCNumber`,`Class`,`TextDecoded`,`ID`,`SenderID`,`SequencePosition`,`Status`,`StatusError`,`TPMR`,`RelativeValidity`,`CreatorID`) values ('2012-05-28 01:16:05','2010-08-04 02:51:57','2012-05-28 01:16:05',NULL,'0069006E007300740061006C00610073006900200073006D0073002D00670061006D006D0075002000730075006B007300650073','085769010001','Default_No_Compression','','+62818445009',-1,'instalasi sms-gammu sukses',7,'',1,'SendingOKNoReport',-1,188,255,''),('2012-05-28 01:16:08','2012-05-28 01:12:52','2012-05-28 01:16:08',NULL,'00530069006C00610068006B0061006E002000430065006B00200061006B0075006E00200061006E00640061002C0020004100640061002000730075007200610074002000640065006E00670061006E0020004E006F006D006F0072002000530075007200610074002000330020','081397932320','Default_No_Compression','','+62818445009',-1,'Silahkan Cek akun anda, Ada surat dengan Nomor Surat 3 ',8,'',1,'SendingOKNoReport',-1,189,255,''),('2012-05-28 12:54:11','2012-05-28 08:30:15','2012-05-28 12:54:11',NULL,'00530069006C00610068006B0061006E002000430065006B00200061006B0075006E00200061006E00640061002C0020004100640061002000730075007200610074002000640065006E00670061006E0020004E006F006D006F007200200053007500720061007400200044003000300031002F0030003800310033003900370039003300320033003200300020','081397932320','Default_No_Compression','','+62816124',-1,'Silahkan Cek akun anda, Ada surat dengan Nomor Surat D001/081397932320 ',12,'',1,'SendingOKNoReport',-1,207,255,''),('2012-05-28 12:54:15','2012-05-28 08:30:15','2012-05-28 12:54:15',NULL,'00530069006C00610068006B0061006E002000430065006B00200061006B0075006E00200061006E00640061002C0020004100640061002000730075007200610074002000640065006E00670061006E0020004E006F006D006F007200200053007500720061007400200044003000300031002F0030003800310033003900370039003300320033003200300020','087791263256','Default_No_Compression','','+62816124',-1,'Silahkan Cek akun anda, Ada surat dengan Nomor Surat D001/081397932320 ',11,'',1,'SendingOKNoReport',-1,208,255,''),('2012-05-28 12:56:10','2012-05-28 12:56:05','2012-05-28 12:56:10',NULL,'00530069006C00610068006B0061006E002000430065006B00200061006B0075006E00200061006E00640061002C0020004100640061002000730075007200610074002000640065006E00670061006E0020004E006F006D006F007200200053007500720061007400200020','087791263256','Default_No_Compression','','+62816124',-1,'Silahkan Cek akun anda, Ada surat dengan Nomor Surat  ',13,'',1,'SendingOKNoReport',-1,209,255,''),('2012-05-28 12:57:24','2012-05-28 12:57:14','2012-05-28 12:57:24',NULL,'00530069006C00610068006B0061006E002000430065006B00200061006B0075006E00200061006E00640061002C0020004100640061002000730075007200610074002000640065006E00670061006E0020004E006F006D006F007200200053007500720061007400200020','081397932320','Default_No_Compression','','+62816124',-1,'Silahkan Cek akun anda, Ada surat dengan Nomor Surat  ',14,'',1,'SendingOKNoReport',-1,210,255,'');

/*Table structure for table `t_agenda_surat_keluar` */

DROP TABLE IF EXISTS `t_agenda_surat_keluar`;

CREATE TABLE `t_agenda_surat_keluar` (
  `nomor` int(11) NOT NULL AUTO_INCREMENT,
  `no_surat` varchar(100) NOT NULL,
  `pengirim_srt_klr` varchar(255) NOT NULL,
  `perihal` varchar(255) NOT NULL,
  `tujuan_srt` varchar(255) NOT NULL,
  `tgl_agd` varchar(100) NOT NULL,
  `isisurat` longtext NOT NULL,
  `status` enum('belum','sudah') NOT NULL DEFAULT 'belum',
  `tujuan` varchar(100) NOT NULL,
  `id_jenis_arsip` int(11) NOT NULL,
  PRIMARY KEY (`nomor`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `t_agenda_surat_keluar` */

insert  into `t_agenda_surat_keluar`(`nomor`,`no_surat`,`pengirim_srt_klr`,`perihal`,`tujuan_srt`,`tgl_agd`,`isisurat`,`status`,`tujuan`,`id_jenis_arsip`) values (6,'Doal','sdad','asdasd','asdasd','asdasd','5d2c4-16.jpg','sudah','asdad',1);

/*Table structure for table `t_agenda_surat_masuk` */

DROP TABLE IF EXISTS `t_agenda_surat_masuk`;

CREATE TABLE `t_agenda_surat_masuk` (
  `no_agenda` varchar(100) NOT NULL,
  `no_file_surat` int(11) NOT NULL,
  `pengirim_surat` varchar(255) NOT NULL,
  `perihal_surat_masuk` varchar(255) NOT NULL,
  `tujuan_srt_masuk` varchar(255) NOT NULL,
  `tgl_agenda` date NOT NULL,
  `tgl_surat` date NOT NULL,
  `nmr_surat` varchar(100) NOT NULL,
  `id_jenis_arsip` int(11) NOT NULL,
  PRIMARY KEY (`no_agenda`),
  UNIQUE KEY `no_file_surat` (`no_file_surat`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `t_agenda_surat_masuk` */

insert  into `t_agenda_surat_masuk`(`no_agenda`,`no_file_surat`,`pengirim_surat`,`perihal_surat_masuk`,`tujuan_srt_masuk`,`tgl_agenda`,`tgl_surat`,`nmr_surat`,`id_jenis_arsip`) values ('00001',1,'PT. Angin Ribut','Surat Edaran Kementrian','Kabid UMUM','2012-11-06','2012-11-21','1234',1),('00002',2,'PT. Maju Mundur','Surat Permohonan','SUBBAG UMUM','2012-11-06','2012-06-12','1762',2);

/*Table structure for table `t_arsip_keluar` */

DROP TABLE IF EXISTS `t_arsip_keluar`;

CREATE TABLE `t_arsip_keluar` (
  `nomor` int(11) NOT NULL,
  `no_surat` varchar(100) NOT NULL,
  `pengirim_srt_klr` varchar(255) NOT NULL,
  `perihal` varchar(255) NOT NULL,
  `tujuan_srt` varchar(255) NOT NULL,
  `tgl_agd` date NOT NULL,
  `isisurat` longtext NOT NULL,
  `status` enum('belum','sudah') NOT NULL,
  PRIMARY KEY (`nomor`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `t_arsip_keluar` */

/*Table structure for table `t_disposisi` */

DROP TABLE IF EXISTS `t_disposisi`;

CREATE TABLE `t_disposisi` (
  `kd_disposisi` int(11) NOT NULL AUTO_INCREMENT,
  `no_surat` varchar(11) NOT NULL,
  `kode_bag` varchar(11) NOT NULL,
  `tindakan` varchar(255) NOT NULL,
  `status` enum('sudah','belum') NOT NULL DEFAULT 'belum',
  `tanggal` date NOT NULL,
  PRIMARY KEY (`kd_disposisi`),
  KEY `no_surat` (`no_surat`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `t_disposisi` */

insert  into `t_disposisi`(`kd_disposisi`,`no_surat`,`kode_bag`,`tindakan`,`status`,`tanggal`) values (1,'1','D002','Tolong Dilihat','sudah','2012-11-06'),(2,'1','D012','Tolong Dipelajari','belum','2012-11-06'),(3,'1','D013','Silahkan Dipeljaro','belum','2012-11-06'),(4,'1','D014','Silahkan Dipeljaro','belum','2012-11-06'),(5,'1','D015','Silahkan Dipeljaro','belum','2012-11-06'),(6,'1','D016','Silahkan Dipeljaro','belum','2012-11-06'),(7,'1','D017','Silahkan Dipeljaro','belum','2012-11-06'),(8,'1','D018','Silahkan Dibalas','belum','2012-11-06'),(9,'1','D019','Silahkan Dipeljaro','belum','2012-11-06'),(10,'1','D020','Silahkan Dipeljaro','belum','2012-11-06');

/*Table structure for table `t_file_arsip_masuk` */

DROP TABLE IF EXISTS `t_file_arsip_masuk`;

CREATE TABLE `t_file_arsip_masuk` (
  `no_file_surat` int(11) NOT NULL AUTO_INCREMENT,
  `nama_file` varchar(255) NOT NULL,
  `ukuran` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `tgl_surat` date DEFAULT NULL,
  PRIMARY KEY (`no_file_surat`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Data for the table `t_file_arsip_masuk` */

insert  into `t_file_arsip_masuk`(`no_file_surat`,`nama_file`,`ukuran`,`type`,`tgl_surat`) values (1,'DAFTAR-PESERTA-PPS-BRI-PALEMBANG-021012.pdf','269159','application/pdf','2012-11-06'),(2,'1544-1-477017766202.pdf','72080','application/pdf','2012-11-06');

/*Table structure for table `t_fungsi` */

DROP TABLE IF EXISTS `t_fungsi`;

CREATE TABLE `t_fungsi` (
  `id_fungsi` varchar(11) NOT NULL,
  `nama_fungsi` varchar(255) NOT NULL,
  PRIMARY KEY (`id_fungsi`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 CHECKSUM=1;

/*Data for the table `t_fungsi` */

insert  into `t_fungsi`(`id_fungsi`,`nama_fungsi`) values ('B009','General Suport'),('B008','Logistik'),('B007','HSE'),('B006','Perizinan'),('B005','EPCC'),('B004','Produksi'),('B003','Pemboran'),('B002','Infrastruktur'),('B001','Project Manager');

/*Table structure for table `t_jenis_arsip` */

DROP TABLE IF EXISTS `t_jenis_arsip`;

CREATE TABLE `t_jenis_arsip` (
  `id_jenis_arsip` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_surat` varchar(255) NOT NULL,
  `id_fungsi` varchar(11) NOT NULL,
  PRIMARY KEY (`id_jenis_arsip`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `t_jenis_arsip` */

insert  into `t_jenis_arsip`(`id_jenis_arsip`,`jenis_surat`,`id_fungsi`) values (1,'Sub Fungsi 1','B005'),(2,'Sub Fungsi 2','B009');

/*Table structure for table `t_tembusan` */

DROP TABLE IF EXISTS `t_tembusan`;

CREATE TABLE `t_tembusan` (
  `no_surat` varchar(11) NOT NULL,
  `tujuan` varchar(255) NOT NULL,
  PRIMARY KEY (`no_surat`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `t_tembusan` */

/*Table structure for table `t_tujuan_disposisi` */

DROP TABLE IF EXISTS `t_tujuan_disposisi`;

CREATE TABLE `t_tujuan_disposisi` (
  `no_surat` varchar(11) NOT NULL,
  `kode_bag` varchar(11) NOT NULL,
  `status` enum('sudah','belum') NOT NULL DEFAULT 'belum',
  PRIMARY KEY (`no_surat`,`kode_bag`),
  KEY `kode_bag` (`kode_bag`),
  KEY `no_surat` (`no_surat`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `t_tujuan_disposisi` */

insert  into `t_tujuan_disposisi`(`no_surat`,`kode_bag`,`status`) values ('1','D001','sudah'),('2','D001','belum');

/*Table structure for table `t_user` */

DROP TABLE IF EXISTS `t_user`;

CREATE TABLE `t_user` (
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `id_fungsi` varchar(11) NOT NULL,
  `level` varchar(50) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `t_user` */

insert  into `t_user`(`username`,`password`,`id_fungsi`,`level`) values ('admin','21232f297a57a5a743894a0e4a801fc3','B002','admin'),('gs','1d8d5e912302108b5e88c3e77fcad378','B009','general user'),('infrastruktur','63825ee55e8931a7c38e7d43a647803b','B002','general user'),('pemboran','91ad838d70db7f53ffd04c506524eb54','B003','general user'),('pm','5109d85d95fece7816d9704e6e5b1279','B001','project manager'),('produksi','edf3017a2946290b95c783bd1a7f0ba7','B004','general user');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
